name := "datasystems_interview"

scalaVersion in ThisBuild := "2.12.2"

lazy val commonSettings = Seq(
  libraryDependencies ++= Seq(
    "com.sksamuel.avro4s" %% "avro4s-core" % "1.6.4"
  )
)

lazy val akkaSettings = Seq(

  libraryDependencies ++= Seq(
    "com.typesafe.akka" %% "akka-actor" % "2.5.2",
    "com.typesafe.akka" %% "akka-slf4j" % "2.5.2",
    "org.log4s" %% "log4s" % "1.3.4",
    "ch.qos.logback" % "logback-classic" % "1.2.3",
    "org.scalamock" %% "scalamock-scalatest-support" % "3.6.0" % Test,
    "com.typesafe.akka" %% "akka-testkit" % "2.5.2" % Test,
    "org.scalatest" %% "scalatest" % "3.0.3" % Test
  )
)


lazy val common = project
  .settings(commonSettings)

lazy val akka = project
  .dependsOn(common)
  .settings(akkaSettings)

