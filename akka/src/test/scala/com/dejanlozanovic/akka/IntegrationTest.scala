package com.dejanlozanovic.akka

import java.net.{InetSocketAddress, ServerSocket}

import com.dejanlozanovic.akka.readers.{ItemReader, UserItemIdReader}
import com.sksamuel.avro4s.AvroInputStream
import com.starcount.common.schema._
import org.scalamock.scalatest.MockFactory
import org.scalatest.{BeforeAndAfterEach, FlatSpec}

import scala.concurrent.Future
import scala.util.Success


class IntegrationTest extends FlatSpec with MockFactory with BeforeAndAfterEach {

  val userItemIds = List(UserItemIds(User(1, "user 1"), List(1, 2, 3)), UserItemIds(User(2, "user 2"), List(1, 3, 5)))
  val items = List(Item(1, "Item 1"), Item(2, "Item 2"), Item(3, "Item 3"), Item(4, "Item 4"), Item(5, "Item 5"))
  val expected = Set(
    UserItem(User(1, "user 1"), Item(1, "Item 1")),
    UserItem(User(1, "user 1"), Item(2, "Item 2")),
    UserItem(User(1, "user 1"), Item(3, "Item 3")),
    UserItem(User(2, "user 2"), Item(1, "Item 1")),
    UserItem(User(2, "user 2"), Item(3, "Item 3")),
    UserItem(User(2, "user 2"), Item(5, "Item 5"))
  )
  val port = 4263
  val remote: InetSocketAddress = new InetSocketAddress("localhost",port)


  var userItemIdReader: UserItemIdReader = _
  var itemReader: ItemReader = _
  var serverSocket:ServerSocket = _

  override def beforeEach(): Unit = {
    userItemIdReader = mock[UserItemIdReader]
    userItemIds.foreach { item =>
      (userItemIdReader.hasNext _).expects().returns(true)
      (userItemIdReader.read _).expects().returns(Success(item))
    }
    (userItemIdReader.hasNext _).expects().returns(false)

    itemReader = mock[ItemReader]
    items.foreach { item =>
      (itemReader.hasNext _).expects().returns(true)
      (itemReader.read _).expects().returns(Success(item))
    }
    (itemReader.hasNext _).expects().returns(false)

    serverSocket = new ServerSocket(port)

  }

  "Integration Test" should "read all items from avro files and send them through socket" in {
    import scala.concurrent.ExecutionContext.Implicits.global

    val future: Future[Unit] = Future(Main.proccessRecords(itemReader,userItemIdReader,remote,1))
    val socket = serverSocket.accept
    val in = socket.getInputStream

    var size = 0
    val buffer: Array[Byte] = new Array(1000000)

    var currentRead = 0
    while(currentRead>=0) {
      currentRead = in.read(buffer,size,buffer.size - size)
      if(currentRead>0){
        size +=currentRead
      }
    }

    val str = new String(buffer.slice(0, size))


    val input = AvroInputStream.data[UserItem](buffer.slice(0, size))

    val actual = input.iterator.toSet

    assert(expected == actual)
    assert(future.isCompleted)
  }
}
