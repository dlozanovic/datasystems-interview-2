package com.dejanlozanovic.akka

import com.dejanlozanovic.akka.readers.ItemReader
import com.starcount.common.schema._
import org.scalamock.scalatest.MockFactory
import org.scalatest.FlatSpec

import scala.util.Success


class DataTransformationsTest extends FlatSpec with MockFactory{


  "ItemIndex" should "read all items from avro file and keep them in sequence" in {
    val expected = List(Item(1,"Item 1"),Item(42,"Answer to the Ultimate Question of Life, The Universe, and Everything"))
    val reader = mock[ItemReader]
    (reader.hasNext _).expects().returns(true)
    (reader.hasNext _).expects().returns(true)
    (reader.hasNext _).expects().returns(false)
    (reader.read _).expects().returns(Success(expected.head))
    (reader.read _).expects().returns(Success(expected.last))

    val actual = DataTransformations.readAllItems(reader)
    assert(actual == expected)
  }

  it should "make index map with key as id and value as item from item sequence" in {
    val items = List(Item(1,"Item 1"),Item(42,"Answer to the Ultimate Question of Life, The Universe, and Everything"))

    val result = DataTransformations.indexItemsById(items)
    assert(result.size == 2)
    assert(result.get(1).get == items.head)
    assert(result.get(42).get == items.last)
  }

  "join operation" should "return sequence of UserItem with matched items for user" in {
    val items = DataTransformations.indexItemsById(List(Item(1,"Item 1"),Item(42,"Answer to the Ultimate Question of Life, The Universe, and Everything")))

    val userItemId = UserItemIds(User(1,"user 1"),List(1,42))

    val result = DataTransformations.joinUserAndItems(userItemId,items)
    assert(result.head == UserItem(userItemId.user,items.get(1).get))
    assert(result.last == UserItem(userItemId.user,items.get(42).get))
  }

  it should "return empty sequence of UserItem" in {
    val items = DataTransformations.indexItemsById(List(Item(1,"Item 1"),Item(42,"Answer to the Ultimate Question of Life, The Universe, and Everything")))

    val userItemId = UserItemIds(User(1,"user 1"),List(13,43,87))

    val result = DataTransformations.joinUserAndItems(userItemId,items)
    assert(result.size == 0)

  }
}
