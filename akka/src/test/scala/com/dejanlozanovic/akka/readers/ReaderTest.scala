package com.dejanlozanovic.akka.readers

import com.starcount.common.schema.{Item, UserItemIds}
import org.scalatest.FlatSpec


class ReaderTest extends FlatSpec {

  "ItemReader" should "read element from avro file" in {
    val reader = new ItemReader("data/items.avro")
    assert(reader.hasNext == true)
    assert(reader.read().get.isInstanceOf[Item] == true)
  }

  "UserItemIds" should "read element from avro file" in {
    val reader = new UserItemIdReader("data/user_item_ids.avro")
    assert(reader.hasNext == true)
    assert(reader.read().get.isInstanceOf[UserItemIds] == true)
  }


}
