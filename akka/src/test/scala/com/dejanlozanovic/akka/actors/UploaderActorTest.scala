package com.dejanlozanovic.akka.actors

import java.net.{InetSocketAddress, ServerSocket}

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit}
import com.dejanlozanovic.akka.actors.messages.NoMoreRecords
import com.sksamuel.avro4s.AvroInputStream
import com.starcount.common.schema.{Item, User, UserItem}
import org.scalatest._


class UploaderActorTest extends TestKit(ActorSystem("TestSystem")) with ImplicitSender
  with FlatSpecLike with Matchers with BeforeAndAfterAll with BeforeAndAfterEach {

  var actor: ActorRef = _
  var server: ServerSocket = _

  override def beforeEach() {
    val port = 9998
    val remote: InetSocketAddress = new InetSocketAddress("localhost", port)
    server = new ServerSocket(port)
    actor = system.actorOf(Props(classOf[UploaderActor], remote))
  }

  override def afterEach(): Unit = server.close()


  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  "UploaderActor" should "connect to server and send some data" in {
    val connection = server.accept()
    val expected = List(UserItem(User(1, "Dejan Lozanovic"), Item(1, "Item 1")), UserItem(User(1, "Dejan Lozanovic"), Item(2, "Item 2")), UserItem(User(2, "Liam"), Item(3, "Item 3")))

    val in = connection.getInputStream
    var size = 0
    val buffer: Array[Byte] = new Array(1000000)

    expected.foreach { element =>
      actor ! element
      size += in.read(buffer, size, buffer.size - size)
    }

    actor ! NoMoreRecords
    connection.close()

    val input = AvroInputStream.data[UserItem](buffer.slice(0, size))
    val actual = input.iterator.toList


    assert(expected == actual)

  }

}
