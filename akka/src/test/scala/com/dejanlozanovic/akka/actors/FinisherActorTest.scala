package com.dejanlozanovic.akka.actors

import java.net.{InetSocketAddress, ServerSocket}

import akka.actor.{ActorRef, ActorSystem, Props, Terminated}
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import com.dejanlozanovic.akka.actors.messages.{NoMoreRecords, WatchMe}
import com.sksamuel.avro4s.AvroInputStream
import com.starcount.common.schema.{Item, User, UserItem}
import org.scalatest._

import scala.concurrent.Future


class FinisherActorTest extends TestKit(ActorSystem("TestSystem")) with ImplicitSender
  with FlatSpecLike with Matchers with BeforeAndAfterAll with BeforeAndAfterEach {


  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  "FinisherActor" should "after all watched actors dies it should stop system" in {
    val actor: ActorRef = system.actorOf(Props(classOf[StubTerminationMethod], testActor))

    val probe1 = TestProbe()
    val probe2 = TestProbe()

    actor ! WatchMe(probe1.ref)
    actor ! WatchMe(probe2.ref)

    system.stop(probe1.ref)
    system.stop(probe2.ref)

    expectMsg("terminate")
  }

}

class StubTerminationMethod(ref:ActorRef) extends FinisherActor {
  override def terminateSystem(): Unit = ref ! "terminate"
}