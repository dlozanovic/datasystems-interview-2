package com.dejanlozanovic.akka.actors

import akka.actor.{ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit}
import com.dejanlozanovic.akka.actors.messages.{GetNextRecord, NoMoreRecords}
import com.dejanlozanovic.akka.readers.UserItemIdReader
import com.starcount.common.schema.{User, UserItemIds}
import org.scalamock.scalatest.MockFactory
import org.scalatest.{BeforeAndAfterAll, FlatSpecLike, Matchers}

import scala.util.{Failure, Success}


class UserItemsReaderActorTest extends TestKit(ActorSystem("TestSystem")) with ImplicitSender
  with FlatSpecLike with Matchers with BeforeAndAfterAll with MockFactory {

  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  "ReaderActor" should "read records from UserItemIdReader and pass them to sender" in {
    val expected = List(UserItemIds(User(1, "user 1"), List(1, 2, 3)), UserItemIds(User(2, "user 1"), List(1, 3, 5)))
    val reader = mock[UserItemIdReader]
    (reader.hasNext _).expects().returns(true)
    (reader.hasNext _).expects().returns(true)
    (reader.hasNext _).expects().returns(true)
    (reader.hasNext _).expects().returns(false)
    (reader.read _).expects().returns(Success(expected.head))
    (reader.read _).expects().returns(Failure(new Exception("keep calm and don't panic!!!")))
    (reader.read _).expects().returns(Success(expected.last))

    val actor = system.actorOf(Props(classOf[UserItemsReaderActor],reader))

    actor ! GetNextRecord
    expectMsg(expected.head)
    actor ! GetNextRecord
    expectMsg(expected.last)
    actor ! GetNextRecord
    expectMsg(NoMoreRecords)
  }

}
