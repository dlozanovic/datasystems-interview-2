package com.dejanlozanovic.akka.actors

import akka.actor.{ActorRef, ActorRefFactory, ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import com.dejanlozanovic.akka.DataTransformations
import com.dejanlozanovic.akka.actors.messages.{GetNextRecord, NoMoreRecords, WatchMe}
import com.starcount.common.schema.{Item, User, UserItemIds}
import org.scalatest._

import scala.concurrent.duration._


class JoinDataActorTest extends TestKit(ActorSystem("TestSystem")) with ImplicitSender
  with FlatSpecLike with Matchers with BeforeAndAfterAll with BeforeAndAfterEach {

  val userItemsId = List(UserItemIds(User(1, "user 1"), List(1, 2, 3)), UserItemIds(User(2, "user 1"), List(1, 3, 42)))
  val items = DataTransformations.indexItemsById(List(Item(1, "Item 1"), Item(2, "Item 2"), Item(3, "Item 3"), Item(42, "Answer to the Ultimate Question of Life, The Universe, and Everything")))


  var reader: TestProbe = _
  var uploader: TestProbe = _
  var finisher: TestProbe = _
  var actor: ActorRef = _

  override def beforeEach() {
    reader = TestProbe("reader")
    uploader = TestProbe("uploader")
    finisher = TestProbe("finisher")

    val maker = (_: ActorRefFactory) => uploader.ref
    actor = system.actorOf(Props(classOf[JoinDataActor], items, reader.ref, finisher.ref, maker))
  }

  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  "JoidDataActor" should "give uploader next useritem  when it request new record" in {
    val userItems = DataTransformations.joinUserAndItems(userItemsId.head, items)
    actor ! userItemsId.head
    actor ! GetNextRecord

    uploader.expectMsg(userItems.head)
  }

  it should "reqest a record from reader if it don't have records to serve" in {
    actor ! GetNextRecord
    reader.expectMsg(GetNextRecord)
    uploader.expectNoMsg(100 millis)

  }

  it should "remember uploader request and give him new useritem when it get new useritemids" in {
    actor ! GetNextRecord
    reader.expectMsg(GetNextRecord)
    uploader.expectNoMsg(100 millis)

    val userItems = DataTransformations.joinUserAndItems(userItemsId.head, items)
    actor ! userItemsId.head
    uploader.expectMsg(userItems.head)

  }

  it should "give all useritems before it request for new useritemsid" in {

    val userItems = DataTransformations.joinUserAndItems(userItemsId.head, items)
    actor ! userItemsId.head

    userItems.foreach { useritem =>
      actor ! GetNextRecord
      uploader.expectMsg(useritem)
      reader.expectNoMsg(10 millis)
    }

    actor ! GetNextRecord
    reader.expectMsg(GetNextRecord)

  }

  it should "notify uploader that there are no more records" in {
    actor ! NoMoreRecords
    actor ! GetNextRecord

    uploader.expectMsg(NoMoreRecords)

  }

  it should "notify finisher when actor is created so it track it and know when to shut down akka system" in {
    finisher.expectMsg(WatchMe(actor))
  }

}
