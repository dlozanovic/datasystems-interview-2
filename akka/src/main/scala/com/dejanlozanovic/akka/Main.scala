package com.dejanlozanovic.akka

import java.io.File
import java.net.InetSocketAddress

import akka.actor.{ActorRefFactory, ActorSystem, Props}
import com.dejanlozanovic.akka.actors.{FinisherActor, JoinDataActor, UploaderActor, UserItemsReaderActor}
import com.dejanlozanovic.akka.readers.{ItemReader, UserItemIdReader}
import org.log4s.getLogger


object Main {
  val logger = getLogger

  def main(args: Array[String]): Unit = {
    if (args.size != 5) {
      println("usage: <items.avro> <user_items_id.avro> <host> <port> <number_of_concurent_uploaders>")
      System.exit(1)
    }

    if (!new File(args(0)).exists()) {
      println(s"${args(0)} does not exists")
      System.exit(1)
    }

    val itemReader: ItemReader = new ItemReader(args(0))

    if (!new File(args(1)).exists()) {
      println(s"${args(1)} does not exists")
      System.exit(1)
    }

    val userItemIdReader: UserItemIdReader = new UserItemIdReader(args(1))

    val remote: InetSocketAddress = new InetSocketAddress(args(2), args(3).toInt)

    val numberOfConnections = Math.max(args(4).toInt, 1)

    proccessRecords(itemReader, userItemIdReader, remote, numberOfConnections)
  }

  def proccessRecords(itemReader: ItemReader, userItemIdReader: UserItemIdReader, remote: InetSocketAddress, numberOfConnections: Int): Unit = {
    logger.info("starting")
    val items = DataTransformations.indexItemsById(DataTransformations.readAllItems(itemReader))
    logger.info("read all items")

    implicit val system = ActorSystem("Akka_TCP_Client")
    implicit val ec = system.dispatcher
    logger.info("actor system started")


    val reader = system.actorOf(Props(classOf[UserItemsReaderActor], userItemIdReader), "reader")
    logger.info("reader actor started")

    val finisher = system.actorOf(Props(classOf[FinisherActor]), "finisher")
    logger.info("finisher actor started")

    val maker = (context: ActorRefFactory) => context.actorOf(Props(classOf[UploaderActor], remote))
    for (i <- 1 to numberOfConnections) {
      system.actorOf(Props(classOf[JoinDataActor], items, reader, finisher, maker))
      logger.info(s"join data actor $i started")
    }

  }
}


