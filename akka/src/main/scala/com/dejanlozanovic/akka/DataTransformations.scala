package com.dejanlozanovic.akka

import com.starcount.common.readers.Avro
import com.starcount.common.schema.{Item, UserItem, UserItemIds}
import org.log4s._

import scala.util.{Failure, Success}


object DataTransformations {
  private[this] val logger = getLogger

  def readAllItems(reader: Avro[Item]): List[Item] = {
    var counter = 0
    var result: List[Item] = Nil
    while (reader.hasNext) {
      counter+=1
      if(counter%1000==0)
        logger.info(s"read $counter records")

      reader.read() match {
        case Success(item) => result = result ::: item :: Nil
        case Failure(e) => logger.error(e)("Problem reading item from reader")
      }
    }
    result
  }

  def indexItemsById(items: List[Item]): Map[Long, Item] = items.map(item => (item.id, item)).toMap


  def joinUserAndItems(userItems: UserItemIds, items: Map[Long, Item]): List[UserItem] = {
    val itemsList: List[(Long, Option[Item])] = userItems.itemIds.map(i => (i, items.get(i)))
    val filterGoodIds = itemsList.partition(_._2 != None)

    filterGoodIds._2.foreach(id => logger.warn(s"There is no item with id: ${id._1} for user: ${userItems.user}"))

    filterGoodIds._1.map(item => UserItem(userItems.user, item._2.get))

  }

}
