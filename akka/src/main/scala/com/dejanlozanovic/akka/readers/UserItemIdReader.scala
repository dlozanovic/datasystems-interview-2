package com.dejanlozanovic.akka.readers

import com.starcount.common.readers.UserItemIds


class UserItemIdReader(_inputPath: String) extends UserItemIds{
  override protected val inputPath: String = _inputPath
}
