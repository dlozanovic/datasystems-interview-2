package com.dejanlozanovic.akka.readers

import com.starcount.common.readers.Item

class ItemReader(_inputPath: String) extends Item {
  override protected val inputPath: String = _inputPath
}
