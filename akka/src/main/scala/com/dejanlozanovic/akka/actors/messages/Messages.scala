package com.dejanlozanovic.akka.actors.messages

import akka.actor.ActorRef

case object GetNextRecord
case object NoMoreRecords
case object FinishedUploading
case class WatchMe(ref:ActorRef)

