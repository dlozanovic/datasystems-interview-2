package com.dejanlozanovic.akka.actors

import akka.actor.{Actor, ActorLogging, ActorRef, Terminated}
import com.dejanlozanovic.akka.actors.messages.{GetNextRecord, WatchMe}

import scala.collection.mutable.ArrayBuffer


class FinisherActor extends Actor with ActorLogging{

  val watched = ArrayBuffer.empty[ActorRef]

  def terminateSystem():Unit = {
    log.info(s"Shuting down, total number of uploaded records: $counter")
    System.exit(0)
  }
  var counter=0

  override def receive: Receive = {
    case WatchMe(ref) =>
      context.watch(ref)
      watched += ref
    case Terminated(ref) =>
      watched -= ref
      log.info(s"one uploader finished. ${watched.size} are still running")
      if (watched.isEmpty)
        terminateSystem()
    case GetNextRecord =>
      counter+=1
      if(counter%1000000==0)
        log.info(s"uploaded $counter records")
  }
}
