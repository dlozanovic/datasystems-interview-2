package com.dejanlozanovic.akka.actors

import java.util.concurrent.Executors

import akka.actor.{Actor, ActorLogging}
import com.dejanlozanovic.akka.actors.messages.{GetNextRecord, NoMoreRecords}
import com.dejanlozanovic.akka.readers.UserItemIdReader
import com.starcount.common.schema.UserItemIds

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}


class UserItemsReaderActor(reader: UserItemIdReader) extends Actor with ActorLogging {

  // single threaded execution context, avro reader does not look like thread safe,
  // and we don;t wish to block actor with long/wait operation
  implicit val ec = ExecutionContext.fromExecutor(Executors.newSingleThreadExecutor())

  override def receive: Receive = {
    case GetNextRecord =>
      val actor = sender()
      Future {
        getNextRecord match {
          case Some(userItemIds) => actor ! userItemIds
          case None => actor ! NoMoreRecords
        }
      }
  }

  def getNextRecord: Option[UserItemIds] = {
    while (reader.hasNext) {
      reader.read match {
        case Success(result) => return Some(result)
        case Failure(e) => log.error(e, "Can't read UserItemIs record")
      }
    }
    None
  }

}
