package com.dejanlozanovic.akka.actors

import java.io.ByteArrayOutputStream
import java.net.InetSocketAddress

import akka.actor.{Actor, ActorLogging, ActorRef}
import akka.io.Tcp.{CommandFailed, Connect, Event}
import akka.io.{IO, Tcp}
import akka.util.ByteString
import com.dejanlozanovic.akka.actors.messages.{FinishedUploading, GetNextRecord, NoMoreRecords}
import com.sksamuel.avro4s.{AvroDataOutputStream, AvroOutputStream}
import com.starcount.common.schema.UserItem


class UploaderActor(remote: InetSocketAddress) extends Actor with ActorLogging {

  var userItemBuffer: List[UserItem] = List()
  var retryItems: List[ByteString] = List()
  var connection: ActorRef = _

  case object DataAck extends Event

  override def receive: Receive = {
    case userItem: UserItem => userItemBuffer = userItemBuffer ::: userItem :: Nil
    case NoMoreRecords => noMoreRecords = true
    case Tcp.ErrorClosed(cause) =>
      log.warning(s"connection closed by: $cause , trying to reconnect")
      connectToServer()
    case Tcp.Connected(remoteAddress, _) =>
      log.info(s"Connected to server $remoteAddress")
      counter = 0
      connection = sender()
      connection ! Tcp.Register(self)
      context.become(connectionEstablished)
      avro = AvroOutputStream.data[UserItem](buffer)
      sendData()

    case a: CommandFailed => a.cmd match {
      case _: Connect => connectToServer()
    }
      log.error(s"command failed $a")

  }


  def connectionEstablished: Receive = {
    case userItem: UserItem => userItemBuffer = userItemBuffer ::: userItem :: Nil
      sendData()
    case DataAck =>
      gotAck = true
      sendData()
    case NoMoreRecords =>
      noMoreRecords = true
      sendData()
    case Tcp.Closed =>
      if (userItemBuffer.nonEmpty || noMoreRecords == false) {
        connectToServer()
        context.unbecome()
      } else {
        context.parent ! FinishedUploading
        context.stop(self)
      }
    case Tcp.Received(data) => log.debug(s"Received something: $data")
    case Tcp.ErrorClosed(cause) =>
      log.warning(s"connection closed by: $cause , trying to reconnect")
      connectToServer()
      context.unbecome()
    case a: CommandFailed =>
      a.cmd match {
        case Tcp.Write(data, _) => retryItems = data :: retryItems
      }
      log.error(s"command failed $a")
  }

  private var noMoreRecords = false
  private var gotAck = true

  private def sendData(): Unit = {
    if (gotAck) {
      if (userItemBuffer.nonEmpty || retryItems.nonEmpty) {
        if (retryItems.nonEmpty) {
          retryItems = retryItems.tail
        } else {
          connection ! Tcp.Write(serializeUserItem(userItemBuffer.head), DataAck)
          userItemBuffer = userItemBuffer.tail
        }

        gotAck = false
      } else if (noMoreRecords) {
        connection ! Tcp.Close
      } else {
        context.parent ! GetNextRecord
      }
    }
  }


  //TODO: for configuration file
  private val MAX_TRIES = 5
  private var counter = 0

  private def connectToServer(): Unit = {
    import context.system
    if (counter < MAX_TRIES) {
      log.debug(s"trying to connect to server try $counter")
      counter += 1
      IO(Tcp) ! Tcp.Connect(remote)
    } else {
      log.error(s"Can't connect to Server at $remote")
      context.parent ! FinishedUploading
      context.stop(self)
    }

  }

  // I assume that no single UserItem will be bigger than 1Mb and i think maybe it is an overkill
  //TODO: move this parameter of 1Mb into a configuration file
  private val buffer = new ByteArrayOutputStream(1000000)
  private var avro: AvroDataOutputStream[UserItem] = _

  private def serializeUserItem(u: UserItem): ByteString = {
    avro.write(u)
    avro.flush()
    val result = ByteString(buffer.toByteArray)
    buffer.reset()
    result
  }


  connectToServer()
}
