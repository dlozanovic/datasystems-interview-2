package com.dejanlozanovic.akka.actors

import akka.actor.{Actor, ActorLogging, ActorRef, ActorRefFactory}
import com.dejanlozanovic.akka.DataTransformations
import com.dejanlozanovic.akka.actors.messages.{FinishedUploading, GetNextRecord, NoMoreRecords, WatchMe}
import com.starcount.common.schema.{Item, UserItem, UserItemIds}


class JoinDataActor(items: Map[Long, Item], reader: ActorRef, finisher: ActorRef, uploadMaker: ActorRefFactory => ActorRef)
  extends Actor with ActorLogging {
  finisher ! WatchMe(self)

  val uploader = uploadMaker(context)

  var userItems: List[UserItem] = List()
  var waitForNextRecord = false
  var noMoreRecords = false

  override def receive: Receive = {
    case a: UserItemIds =>
      userItems = userItems ::: DataTransformations.joinUserAndItems(a, items)
      if (userItems.isEmpty) {
        reader ! GetNextRecord
      } else if (waitForNextRecord) {
        sendUserItemToUploader()
        waitForNextRecord = false
      }

    case GetNextRecord =>
      if (userItems.nonEmpty) {
        sendUserItemToUploader()
      } else if (noMoreRecords) {
        uploader ! NoMoreRecords
      } else {
        waitForNextRecord = true
        reader ! GetNextRecord
      }
      finisher ! GetNextRecord
    case NoMoreRecords =>
      noMoreRecords = true
      if (waitForNextRecord) {
        uploader ! NoMoreRecords
      }
    case FinishedUploading =>
      context.stop(self)
  }

  def sendUserItemToUploader() = {
    uploader ! userItems.head
    userItems = userItems.tail
  }


}
